package controller;

import java.util.Collections;
import java.util.List;

import Service.SummeryService;
import View.DisplayData;
import comparator.GetPostSortComperator;
import model.Summery;

public class Driver {
	
	public static void main(String [] args) {
		if(args.length<1) {
			System.out.println("No arguments given");
			return;
		}
		String filePath=args[0];
		boolean sort=false;
		if(args.length == 2 && args[1].equalsIgnoreCase("--sort")){
			sort=true;
		}
		SummeryService summeryService = new SummeryService();
		DisplayData display =new DisplayData();
		List<Summery> summeries = summeryService.getAllSummeries(filePath);
		if(sort) {
			Collections.sort(summeries,new GetPostSortComperator());
		}
		display.display(summeries);
		
	}

}
