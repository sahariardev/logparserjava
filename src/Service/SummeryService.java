package Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Request;
import model.Summery;

public class SummeryService {
	
	private final RequestService requestService;
	private final Util util;
	public SummeryService() {
		this.requestService = new RequestService();
		this.util= new Util();
	}
	

	public List<Summery> getAllSummeries(String filePath){
		List<Summery> summeries = new ArrayList<Summery>();
		for(int c=0;c<24;c++){
        	summeries.add(new Summery(c));
        }

		File file = new File(filePath);
		try {

			Scanner sc = new Scanner(file);
	        while(sc.hasNextLine())
	        {
	        	String line =sc.nextLine();
	        	if(util.isValidLine(line))
	        	{
	        		Request request= requestService.createRequest(line);
	        		if(request!=null)
	        		{
	        			int time=Integer.parseInt(request.getTime().split(":")[0]);
	        			Summery summery =summeries.get(time);
	        			summery.addRequest(request);
	        		}
	        	}
	        	
	        }
		}catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		return summeries;
	}
	
	
	
}
