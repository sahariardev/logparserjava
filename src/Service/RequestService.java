package Service;

import model.Request;

public class RequestService {
	
	 private final Util util;
	 public RequestService() {
		 this.util = new Util();
	 }

	 public Request createRequest(String line) {
	        if(!util.isConvertableToNumber(util.getResponseTime(line))) return null;    
	    	Request request = new Request();
	    	request.setTime(util.getTime(line));
	    	request.setUri(util.getUri(line));
	    	request.setType(util.getRequestedMethod(line));
	    	request.setResponseTime(Integer.parseInt(util.getResponseTime(line)));
	    	
	    	return request;
	    	
	    }
}
