package Service;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Request;

public class Util {
	
	private final String regexForValidation=".*[0-2][0-9]:[0-6][0-9]:[0-6][0-9].*URI=\\[.*\\], [GP], time=.*";
	private final String regexForTime="[0-2][0-9]:[0-6][0-9]:[0-6][0-9]";
	private final String regexForUri="URI=\\[.*\\]";
	private final String regexForRequestMethodWithTime="[GP], time=.*ms";
	private final String regexForRequestMethod="[GP]";
	private final String regexForResponseTime="time=.*ms";

    
    
    public boolean isConvertableToNumber(String line) {
    	try {
    		Integer.parseInt(line);
    		return true;
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		return false;
    	}
    }
	
	public boolean isValidLine(String line) {
		return Pattern.matches(regexForValidation, line);
	}
	public String getTime(String line) {
		return getData(regexForTime,line);
	}
	
	public String getUri(String line) {
		return getData(regexForUri,line).replace("URI=[", "").replace("]", "");
		
	}
	public String getRequestedMethod(String line) {
		String requestedmethodWithTime = getData(regexForRequestMethodWithTime,line);
		return getData(regexForRequestMethod,requestedmethodWithTime);
	}
	public String getResponseTime(String line) {
		return getData(regexForResponseTime,line).replaceAll("\\D", "");
	}
	
	public String getData(String regex, String data) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(data);
		if (matcher.find()) {
		    return matcher.group(0);
		}
		else {
			return null;	
		}		
	}

}
