package comparator;

import java.util.Comparator;

import model.Summery;

public class GetPostSortComperator implements Comparator<Summery> {

	@Override
	public int compare(Summery o1, Summery o2) {
		
		if(o1.getTotalGet()<o2.getTotalGet())
		{
			return 1;
		}
		else if(o1.getTotalGet()>o2.getTotalGet())
		{
			return -1;
		}
		else
		{
			if(o1.getTotalPost()<o2.getTotalPost())
			{
				return 1;
			}
			else if(o1.getTotalPost()>o2.getTotalPost())
			{
				return -1;
			}
			else return 0;
		}
	}

}
