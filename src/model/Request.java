package model;

public class Request {

	private String type;
	private String time;
	private String uri;
	private int responseTime;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public int getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(int responseTime) {
		this.responseTime = responseTime;
	}
	
	
	@Override
	public String toString() {
		return "Request [type=" + type + ", time=" + time + ", uri=" + uri + ", responseTime=" + responseTime + "]";
	}
	
	
	
}
