package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Summery {

	private List<Request> requests= new ArrayList<Request>();
	private Map<String,Integer> uris= new HashMap<String,Integer>();
	private int totalGet;
	private int totalPost;
	private int totalUniqueUri;
	private int totalResponseTime;
	private String time;
	
	public Summery(int number) {
		
		if(number<11) {
			if(number == 0) {
				time=number+12+":00am-"+1+":00am";
			}
			else {
				time=number+":00am-"+(number+1)+":00am";
			}
		}
		else if(number == 11) {
			time="11:00am-12:00pm";
		}
		else if(number == 23) {
			time="11:00pm-12:00am";
		}
		else {
			if(number == 12) {
				time=12+":00pm-"+1+":00pm";
			}
			else {
				time=(number%12)+":00pm-"+((number%12)+1)+":00pm";
			}
		}
		
	}
	
	public List<Request> getRequests() {
		return requests;
	}
	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}
	public Map<String, Integer> getUris() {
		return uris;
	}
	public void setUris(Map<String, Integer> uris) {
		this.uris = uris;
	}
	public int getTotalGet() {
		return totalGet;
	}
	public void setTotalGet(int totalGet) {
		this.totalGet = totalGet;
	}
	public int getTotalPost() {
		return totalPost;
	}
	public void setTotalPost(int totalPost) {
		this.totalPost = totalPost;
	}
	public int getTotalUniqueUri() {
		return totalUniqueUri;
	}
	public void setTotalUniqueUri(int totalUniqueUri) {
		this.totalUniqueUri = totalUniqueUri;
	}
	public int getTotalResponseTime() {
		return totalResponseTime;
	}
	public void setTotalResponseTime(int totalResponseTime) {
		this.totalResponseTime = totalResponseTime;
	}	
	public void  addRequest(Request request)
	{
		if(!uris.containsKey(request.getUri())){
		   
			uris.put(request.getUri(),1);
			totalUniqueUri++;
			
		}
		if(request.getType().equalsIgnoreCase("G")){
			totalGet++;
		}
		else if(request.getType().equalsIgnoreCase("P")){
			totalPost++;
		}
		totalResponseTime = totalResponseTime+request.getResponseTime();
        requests.add(request);
	}
	public String getByPost()
	{
		return totalGet+"/"+totalPost;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return time+" "+getByPost()+" "+totalUniqueUri+" "+totalResponseTime+"ms";
	}
	
	
}
